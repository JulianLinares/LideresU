//#[derive(Debug)]
extern crate git2;
use git2::build::RepoBuilder;
// use git2::{RemoteCallbacks, Progress, FetchOptions,CheckoutBuilder};
use std::path::Path;


fn main() {
    //define path the repo
    let url = "https://JulianLinares@gitlab.com/JulianLinares/LideresU.git";
    let path = "repo/";
    //define where save the repo
    let save = Path::new(path);
    //clone repo
    let builder = RepoBuilder::new().clone(url, save);
    match builder {
        Ok(ok) => {
            let dir = ok.path();
            println!("repo clonado en el directorio {:?}", dir);
        }
        Err(e) => {
            println!("Repo no clonado {:?}", e);
        }
    }
}
